# SpotifyApp

El proyecto corre en [Angular CLI](https://github.com/angular/angular-cli) version 10.0.4.

## Development server

Para ejecutarlo se necesita  `ng serve` y luego ingresar a `http://localhost:4200/`

## Como Funciona

1. Debes de crear un Token haciendo una peticiOn POST a la siguiente Ruta `https://accounts.spotify.com/api/token` Se recomienda usar POSTMAN para esta acciOn

- x-www-form-urlencoded
- Valores [key - Value]
  - `grant_type`      `client_credentials`
  - `client_id`       `2cbf138562cb49a3a444233a9a05a718`
  - `client_secret`   `1c415263198e4efb8c4c29ab67fc0e3a`

El Resultado esperado es el siguiente

{
    "access_token": "BQCwqbvuFhmeKWO4CPq3U7gHFpHoI-jZS4KgEQcveSvpAFR2XluPEe7hAc7mceBDwngSH_yDR9Hz4r0_HRE",
    "token_type": "Bearer",
    "expires_in": 3600,
    "scope": ""
}