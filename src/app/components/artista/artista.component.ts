import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: [
  ]
})
export class ArtistaComponent {

  artist: any = {};
  loadingArtist: boolean;
  topTracks: any;

  constructor( private router: ActivatedRoute,
               private spotify: SpotifyService ) { 
    this.router.params.subscribe( params => {
      this.getArtist( params['id'] );
      this.getTopTracks( params['id'] );
    })
  }

  getArtist( id: string ) {
    this.loadingArtist = true;
    this.spotify.getArtist( id )
          .subscribe( artist => {
            this.loadingArtist = false;
            this.artist = artist;
          })
  }

  getTopTracks( id: string ) {
    this.spotify.getTopTracks( id )
                .subscribe( TopTracks => {
                  this.topTracks = TopTracks
                  console.log(TopTracks)
                })
  }
}
