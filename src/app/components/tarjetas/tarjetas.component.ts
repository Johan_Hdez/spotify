import { Component, OnInit, Input } from '@angular/core';
import { Router} from '@angular/router'

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styles: [
  ]
})
export class TarjetasComponent {

  @Input() items: any[] = []
  constructor( private router: Router) { }

  viewArtists( item: any) {
    let artistsId;
    if (item.type === 'artist') {
      artistsId = item.id;
    } else {
      artistsId = item.artists[0].id;
    }
    this.router.navigate([ '/artists', artistsId])
  }

}
